surface.CreateFont("SpawnMenuTitle", {
    font = "Helvetica",
    size = 32,
    weight = 800
})

local NAV_BAR = {
    Init = function(self)
        self:Dock(TOP)
        self:SetHeight(38)
        self:DockMargin(30, 0, 30, 2)

        local building = vgui.Create("DButton", self)
        building:Dock(LEFT)
        building:DockMargin(3, 3, 3, 3)
        building:SetSize(32, 32)
        building.SetPos(10, 10)
        building:SetContentAlignment(5)
        building.DoClick = function()
            chat.AddText("building")
        end
    end,

    Setup = function(self, ply)
        self.Player = ply
        self.Avatar:SetPlayer(ply)
        self.Think(self)
    end,

    Think = function(self)

    end,

    Paint = function(self, w, h)

    end
}

NAV_BAR = vgui.RegisterTable(NAV_BAR, "DPanel")

local SPAWN_MENU = {
    Init = function(self)
        vgui.CreateFromTable(NAV_BAR)
    end,

    PerformLayout = function(self)
        self:SetSize(ScrW() - 200, ScrH() - 200)
        self:SetPos(50, 50)
    end,

    Paint = function(self, w, h)
        draw.RoundedBox(8, 0, 0, w, h, Color(255, 255, 255, 240))
    end,

    Think = function(self, w, h)
    end
}

SPAWN_MENU = vgui.RegisterTable(SPAWN_MENU, "EditablePanel")

function GM:OnSpawnMenuOpen()
    if (!IsValid(Spawnmenu)) then
        Spawnmenu = vgui.CreateFromTable(SPAWN_MENU)
    end
    if (IsValid(Spawnmenu)) then
        Spawnmenu:Show()

        // Lines to enable and disable the mouse
        Spawnmenu:MakePopup()
        //Scoreboard:SetKeyboardInputEnabled(true)
    end
end

function GM:OnSpawnMenuClose()
    if (IsValid(Spawnmenu)) then
        Spawnmenu:Hide()
    end
end
