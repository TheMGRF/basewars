AddCSLuaFile()
local meta = FindMetaTable("Player") --Get the meta table of player

-- Player first spawn
function FirstSpawn(ply)
	local cash = ply:GetPData("money")

	if cash == nil then
		ply:SetPData("money", starting_money) -- Save
		ply:SetMoney(starting_money) -- Networked ints
	else
		ply:SetMoney(cash) -- If not, set the networked ints to what we last saved
	end

	ply:AllowFlashlight(true);
end
hook.Add("PlayerInitialSpawn", "playerInitialSpawn", FirstSpawn)

function GM:PlayerSpawn(ply)
	-- Give player starting weapons
	ply:Give("weapon_physcannon")
	ply:Give("weapon_physgun")
	ply:Give("hands")
	ply:Give("gmod_camera")
	ply:Give("gmod_tool")

	ply:SwitchToDefaultWeapon()

	ply:SetupHands()
end
-- Choose the model for hands according to their player model.
function GM:PlayerSetHandsModel( ply, ent )

	local simplemodel = player_manager.TranslateToPlayerModelName( ply:GetModel() )
	local info = player_manager.TranslatePlayerHands( simplemodel )
	if ( info ) then
		ent:SetModel( info.model )
		ent:SetSkin( info.skin )
		ent:SetBodyGroups( info.body )
	end

end

-- Player disconnect
function PlayerDisconnect(ply)
	print("Player Disconnected: Money saved to disk")
	ply:SaveMoney()
	ply:SaveMoneyTXT()
end
hook.Add("PlayerDisconnected", "playerDisconnectedEvent", PlayerDisconnect)

-- Add money to a players balance
function meta:AddMoney(amount)
	local current_cash = self:GetMoney()
	self:SetMoney( current_cash + amount )
end

-- Set a players balance
function meta:SetMoney(amount)
	self:SetNetworkedInt( "Money", amount )
	self:SaveMoney()
end


function meta:SaveMoney()
	local cash = self:GetMoney()
	self:SetPData("money", cash)
end

-- Save players balance to disk
function meta:SaveMoneyTXT()
	file.Write(gmod.GetGamemode().Name .."/Money/".. string.gsub(self:SteamID(), ":", "_") ..".txt", self:GetMoney())
end

-- Reduce a players balance
function meta:TakeMoney(amount)
   self:AddMoney(-amount)
end

-- Get the players balance
function meta:GetMoney()
	return self:GetNetworkedInt("Money")
end
