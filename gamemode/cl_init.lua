include("shared.lua")

-- Economy Manager?
-- include("economy_manager/cl_economy_manager.lua")

-- Player Manager
AddCSLuaFile("player/sh_player.lua")
include("player/sh_player.lua")

-- Hud Stuff
include("player/hud/removeOld.lua")
include("player/hud/playerInfo.lua")
include("player/hud/scoreboard.lua")
include("player/hud/spawnmenu.lua")

include("player/hud/createTeam.lua")


--Hooks
--GUIs
hook.Add("OnSpawnMenuOpen", "openSpawnMenu", function()
    --print("test")
    vgui.CreateFromTable(SPAWN_MENU)
end)
