--include("../player/hud/createTeam.lua")

util.AddNetworkString("teamCreator")

function teamCommand(ply, text, team)
    if (text == "!createteam" || text == "!ct") then
        -- Send networking message to open vgui panel
        net.Start("teamCreator")
            net.WriteEntity(ply)
        net.Send(ply)
        return false
    elseif (text == "!leaveteam" || text == "!lt") then
        if (ply:Team() != 0) then
            ply:SetTeam(0)
            ply:SetPlayerColor(Vector(0.1, 0.1, 0.1))
            ply:ChatPrint("You left your team.")

            -- TODO: Send leave team network msg
            net.Start("leaveTeam")
                net.WriteEntity(ply)
            net.Send(ply)
        else
            ply:ChatPrint("You are not in any team.")
        end
        return false
    end
end
