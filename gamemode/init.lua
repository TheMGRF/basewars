AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

--[[ Extra Scripts
AddCSLuaFile("economy_manager/cl_economy_manager.lua")
include("economy_manager/sv_economy_manager.lua")
]]--

-- Player Manager
AddCSLuaFile("player/sh_player.lua")
include("player/sh_player.lua")

-- Hud Elements
AddCSLuaFile("player/hud/removeOld.lua")
AddCSLuaFile("player/hud/playerInfo.lua")
AddCSLuaFile("player/hud/scoreboard.lua")
AddCSLuaFile("player/hud/createTeam.lua")
AddCSLuaFile("player/hud/spawnmenu.lua")

include("prop_selection.lua")
--include("player/hud/spawnmenu.lua")

-- Commands
include("commands/generic_commands.lua")
include("commands/team_command.lua")

-- Variables
starting_money = 1000
totalTeams = 0

-- debug print cash
function PrintCash(ply)
	ply:ChatPrint("Your cash is: £" .. ply:GetMoney())
	ply:AddMoney(10)
end

concommand.Add("getCash", PrintCash)

-- Hooks
-- Command hooks
hook.Add("PlayerSay", "teamCommand", teamCommand)
hook.Add("PlayerSay", "genericCommands", genericCommands)

-- Team creation hook
util.AddNetworkString("teamCreated")
util.AddNetworkString("cl_teamCreated")
net.Receive("teamCreated", function()
    local teamName = net.ReadString()
	local teamColour = net.ReadColor()
	local ply = net.ReadEntity()

	totalTeams = totalTeams + 1
	team.SetUp(totalTeams, teamName, teamColour, false)
	print(ply:Name() .. " just created team " .. team.GetName(totalTeams) .. " with id " .. totalTeams)
	ply:SetTeam(totalTeams)
	team.SetColor(totalTeams, teamColour)
	ply:SetPlayerColor(Vector(teamColour.r/255, teamColour.g/255, teamColour.b/255))

	-- Send message to player for team name in hud
	net.Start("cl_teamCreated")
		net.WriteString(teamName)
	net.Send(ply)
end)

util.AddNetworkString("getTeamName")
util.AddNetworkString("sendTeamName")
util.AddNetworkString("leaveTeam")

net.Receive("getTeamName", function(len, ply)
	local teamName = team.GetName(net.ReadInt)
	print("SV_TEAM NAME: " .. teamName)

	net.Start("sendTeamName")
		net.WriteString(teamName)
	net.Send(ply)
end)

-- Stuff from vid
function GM:PlayerConnect(name, ip)
    print("Player " .. name .. " connected with the IP (" .. ip .. ")")
end

function GM:PlayerInitialSpawn(ply)
    print("Player " .. ply:Name() .. " spawned")

	ply:SetModel( "models/player/Group01/male_0" .. math.random(9) .. ".mdl" )

	color = Color(100, 100, 100)
	ply:SetPlayerColor(Vector(color.r/255, color.g/255, color.b/255))
end
