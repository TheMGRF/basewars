net.Receive("teamCreator", function()
    ply = net.ReadEntity()
    if (ply:Team() == 0) then
        ply:ChatPrint("Creating team...")
        openTeamCreator()
    else
        chat:AddText(Color(255, 0, 0), "You are already in a team. (/leaveteam)")
    end
end)

-- TODO: Create some custom fonts to make the labels bigger
surface.CreateFont("Labels", {
    font = "Helvetica",
    size = 22,
    weight = 800
})

surface.CreateFont("Buttons", {
    font = "Helvetica",
    size = 18,
    weight = 900
})

function openTeamCreator()
    local frame = vgui.Create("DFrame")
    frame:SetSize(300, 400)
    frame:Center()
    frame:SetVisible(true)
    frame:MakePopup()
    frame:SetDraggable(false)
    frame:SetBackgroundBlur(true)
    frame:SetPaintShadow(true)

    frame.Paint = function(s, w, h)
        draw.RoundedBox(10, 0, 0, w, h, Color(0, 0, 0, 200))
    end

    frame:SetTitle("Team Creator")

    -- Creator intro
    local intro = vgui.Create("DLabel", frame)
    intro:SetPos(10, 30)
    intro:SetText("Welcome to the team creation page. Here you can create\nyour own team for Basewars! Make sure to select a name\nand colour that you like.")
    intro:SizeToContents()

    -- Team name label
    local teamNameLbl = vgui.Create("DLabel", frame)
    teamNameLbl:SetPos(10, 73)
    teamNameLbl:SetFont("Labels")
    teamNameLbl:SetText("Team Name")
    teamNameLbl:SizeToContents()

    -- Team name text entry box
    local teamNameEntry = vgui.Create("DTextEntry", frame)
    teamNameEntry:SetPos(10, 95)
    teamNameEntry:SetSize(frame:GetWide() - 20, 40)
    teamNameEntry:SetFont("Labels")
    -- Placeholder text
    --teamNameEntry:SetText("Awesome team name")
    -- Option to allow enter functionality
    --[[teamNameEntry.OnEnter = function(self)
        chat.AddText(Color(0, 255, 0), "Successfully created team: " .. teamNameEntry:GetValue())
        BroadcastLua("chat.AddText(Color(0, 255, 0), ply:GetNick() .. 'Created a new team called: ', teamColour, teamNameEntry:GetValue())")
    end]]--

    -- Team colour label
    local teamColourLbl = vgui.Create("DLabel", frame)
    teamColourLbl:SetPos(10, 140)
    teamColourLbl:SetFont("Labels")
    teamColourLbl:SetText("Team Colour")
    teamColourLbl:SizeToContents()

    -- Team colour picker
    local teamColourPicker = vgui.Create("DRGBPicker", frame)
    teamColourPicker:SetPos(10, 165)
    teamColourPicker:SetSize(29, 150)

    -- Team colour cube
    local teamColourCube = vgui.Create("DColorCube", frame)
    teamColourCube:SetPos(42, 165)
    teamColourCube:SetSize(frame:GetWide() - 52, 150)
    teamColourCube:SetColor()

    -- When the picked color is changed...
    function teamColourPicker:OnChange( col )

    	-- Get the hue of the RGB picker and the saturation and vibrance of the color cube
    	local h = ColorToHSV( col )
    	local _, s, v = ColorToHSV( teamColourCube:GetRGB() )

    	-- Mix them together and update the color cube
    	col = HSVToColor( h, s, v )
    	teamColourCube:SetColor( col )

    	-- Lastly, update the background color and label
    	UpdateColors( col )

    end

    -- Update background color and label
    --function teamColourCube:OnUserChanged( col )

    --end

    -- Updates display colors, label, and clipboard text
    function UpdateColors(col)
    	teamColourCube:SetColor( col )
    end

    -- Create team button
    local createBtn = vgui.Create("DButton", frame)
    createBtn:SetPos(150 - 60, 325)
    createBtn:SetSize(120, 30)
    createBtn:SetFont("Buttons")
    createBtn:SetText("Create Team")
    createBtn.DoClick = function()
        teamName = teamNameEntry:GetValue()
        --teamColour = Color(teamColourCube:GetRGB())

        r,g,b,a = 0
        for k ,v in pairs(teamColourCube:GetRGB()) do
            if (k == "r") then
                r = v
            elseif (k == "b") then
                b = v
            elseif (k == "g") then
                g = v
            elseif (k == "a") then
                a = v
            end
        end
        teamColour = Color(r, g, b, a)

        -- Send net message to server to create team
        net.Start("teamCreated")
        net.WriteString(teamName)
        net.WriteColor(teamColour)
        net.WriteEntity(ply)
        net.SendToServer()

        frame:Close()
        chat.AddText(Color(0, 255, 0), "Successfully created team: ", teamColour,teamName)
        BroadcastLua("chat.AddText(Color(0, 255, 0), ply:Nick() .. 'Created a new team called: ', teamColour, teamName)")
    end

    -- Cancel team creation
    local cancelBtn = vgui.Create("DButton", frame)
    cancelBtn:SetPos(150 - 60, 360)
    cancelBtn:SetSize(120, 30)
    cancelBtn:SetFont("Buttons")
    cancelBtn:SetText("Cancel")
    cancelBtn.DoClick = function()
        frame:Close()
    end
end
