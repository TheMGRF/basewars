surface.CreateFont("ScoreboardTitle", {
    font = "Helvetica",
    size = 32,
    weight = 800
})

surface.CreateFont("ScoreboardPlayer", {
    font = "Helvetica",
    size = 22,
    weight = 800
})

local PLAYER_LINE_TITLE = {
    Init = function(self)
        self.Players = self:Add("DLabel")
        self.Players:Dock(FILL)
        self.Players:SetFont("ScoreboardPlayer")
        self.Players:SetTextColor(Color(255, 255, 255))
        self.Players:DockMargin(0, 0, 0, 0)

        self.Ping = self:Add("DLabel")
        self.Ping:Dock(RIGHT)
        self.Ping:SetFont("ScoreboardPlayer")
        self.Ping:SetTextColor(Color(255, 255, 255))
        self.Ping:DockMargin(0, 0, 20, 0)
		self.Ping:SetText("Ping")

        self.Score = self:Add("DLabel")
        self.Score:Dock(RIGHT)
        self.Score:SetFont("ScoreboardPlayer")
        self.Score:SetTextColor(Color(255, 255, 255))
        self.Score:DockMargin(0, 0, 20, 0)
		self.Score:SetText("Score")

		self.Money = self:Add("DLabel")
        self.Money:Dock(RIGHT)
        self.Money:SetFont("ScoreboardPlayer")
        self.Money:SetTextColor(Color(255, 255, 255))
        self.Money:DockMargin(0, 0, 20, 0)
		self.Money:SetText("Money")

        self:Dock(TOP)
        self:DockPadding(3, 3, 3, 3)
        self:SetHeight(38)
        self:DockMargin(10, 0, 10, 2)

        self:SetZPos(-8000)
    end,

    Think = function(self)
        playerCount = 0

        for k, v in pairs(player.GetAll()) do
            playerCount = playerCount + 1
        end

        self.Players:SetText("Players (" .. playerCount .. ")")
    end,

    Paint = function(self, w, h)
        draw.RoundedBox(4, 0, 0, w, h, Color(50, 50, 50, 175))
    end
}

PLAYER_LINE_TITLE = vgui.RegisterTable(PLAYER_LINE_TITLE, "DPanel")

local PLAYER_LINE = {
    Init = function(self)
        self.AvatarButton = self:Add("DButton")
        self.AvatarButton:Dock(LEFT)
        self.AvatarButton:DockMargin(3, 3, 3, 3)
        self.AvatarButton:SetSize(32, 32)
        self.AvatarButton:SetContentAlignment(5)
        self.AvatarButton.DoClick = function()
            self.Player:ShowProfile()
        end

        self.Avatar = vgui.Create("AvatarImage", self.AvatarButton)
        self.Avatar:SetSize(32, 32)
        self.Avatar:SetMouseInputEnabled(false)

        self.Name = self:Add("DLabel")
        self.Name:Dock(LEFT)
        self.Name:SetContentAlignment(5)
        --self.Name:SetWidth(135)
        --self.Name:SizeToContents()
        self.Name:SetFont("ScoreboardPlayer")
        self.Name:SetTextColor(Color(255, 255, 255))
        self.Name:DockMargin(0, 0, 5, 0)

        self.Scan = self:Add("DButton")
        self.Scan:Dock(LEFT)
        self.Scan:DockMargin(5, 5, 5, 5)
        self.Scan:SetSize(45, 16)
        self.Scan:SetContentAlignment(5)
        self.Scan:SetText("Scan")
        self.Scan.DoClick = function()
            chat.AddText(Color(255, 0, 0, 150), self.Player:GetName() .. " was scanned")

            sound.Play("UI/buttonrollover.wav", ply:GetPos())
            sound.Play("buttons/button17.wav", ply:GetPos())
            sound.Play("npc/scanner/scanner_scan" .. math.random(1, 5) .. ".wav", self.Player:GetPos(), 100)
            --[[
            npc/scanner/scanner_scan1.wav
            npc/scanner/scanner_scan2.wav
            npc/scanner/scanner_scan4.wav
            npc/scanner/scanner_scan5.wav
            ]]--
        end

        --[[self.Scan = vgui.Create("ScanButton", self.ScanButton)
        self.Scan:SetSize(32, 32)
        self.Scan:SetMouseInputEnabled(false)]]--

        -- Mute button
        self.MutePanel = self:Add("DPanel")
        self.MutePanel:SetSize(36, self:GetTall())
        self.MutePanel:Dock(RIGHT)
        function self.MutePanel:Paint(w, h)
            draw.RoundedBox(0, 0, 0, w, h, Color(50, 50, 50, 150))
        end

		-- Mute button
        self.Mute = self.MutePanel:Add("DImageButton")
        self.Mute:SetSize(32, 32)
        self.Mute:Dock(FILL)
        self.Mute:SetContentAlignment(5)

		-- Ping Display
        self.Ping = self:Add("DLabel")
        self.Ping:Dock(RIGHT)
        self.Ping:DockMargin(0, 0, 2, 0)
        self.Ping:SetWidth(50)
        self.Ping:SetFont("ScoreboardPlayer")
        self.Ping:SetTextColor(Color(255, 255, 255))
        self.Ping:SetContentAlignment(5)

        self.Score = self:Add("DLabel")
        self.Score:Dock(RIGHT)
        self.Score:SetFont("ScoreboardDefault")
        self.Score:SetTextColor(Color(255, 255, 255))
        self.Score:DockMargin(0, 0, 20, 0)
        self.Score:SetContentAlignment(5)

        self.Money = self:Add("DLabel")
        self.Money:Dock(RIGHT)
        self.Money:SetFont("ScoreboardDefault")
        self.Money:SetTextColor(Color(255, 255, 255))
        self.Money:DockMargin(0, 0, 20, 0)
        self.Money:SetContentAlignment(5)

        self:Dock(TOP)
        self:SetHeight(38)
        self:DockMargin(10, 0, 10, 2)
    end,

    Setup = function(self, ply)
        self.Player = ply
        self.Avatar:SetPlayer(ply)
        self.Think(self)
    end,

    Think = function(self)
        if (!IsValid(self.Player)) then
            self:SetZPos(9999)
            self:Remove()
            return
        end

        -- Kills check
        if (self.NumKills == nil || self.NumKills != self.Player:Frags()) then
            self.NumKills = self.Player:Frags()
            self.Score:SetText(self.NumKills)
        end

        if (self.PName == nil || self.PName != self.Player:Nick()) then
            self.PName = self.Player:Nick()
            self.Name:SetText(self.PName)
            self.Name:SizeToContents()
        end

        if (self.NumPing == nil || self.NumPing != self.Player:Ping()) then
            self.NumPing = self.Player:Ping()
            self.Ping:SetText(self.NumPing)
        end

		-- Totally inefficient way of updating money
		self.Money:SetText("$" .. commarise(self.Player:GetNetworkedInt("Money")))

        if (self.Muted == nil || self.Muted != self.Player:IsMuted()) then
            self.Muted = self.Player:IsMuted()
            if (self.Muted) then
                self.Mute:SetImage("icon32/muted.png")
            else
                self.Mute:SetImage("icon32/unmuted.png")
            end

            self.Mute.DoClick = function()
                self.Player:SetMuted(!self.Muted)
                sound.Play("buttons/button18.wav", ply:GetPos())
            end
        end

        -- TODO: Might have to do something here for showing a header for teams
        self:SetZPos(self.Player:Team())
    end,

    Paint = function(self, w, h)
        if (!IsValid(self.Player)) then
            return
        end

        if (self.Player:Alive()) then
            colours = self.Player:GetPlayerColor()
            draw.RoundedBox(4, 0, 0, w, h, Color(colours.r*255, colours.g*255, colours.b*255, 175))
        else
            draw.RoundedBox(4, 0, 0, w, h, Color(10, 10, 10, 175))
            return
        end
    end
}

PLAYER_LINE = vgui.RegisterTable(PLAYER_LINE, "DPanel")

local SCORE_BOARD = {
    Init = function(self)
        self.Header = self:Add("Panel")
        self.Header:Dock(TOP)
        self.Header:SetHeight(50)

        // Scoreboard Title
        self.Name = self.Header:Add("DLabel")
        self.Name:SetFont("ScoreboardTitle")
        self.Name:SetTextColor(Color(255, 255, 255, 255))
        self.Name:Dock(TOP)
        self.Name:SetHeight(50)
        self.Name:SetContentAlignment(5)
        self.Name:SetExpensiveShadow(3, Color(0, 0, 0, 200))
        self.Name:DockMargin(0, 0, 0, 0)

        // Players with scroll bar
        self.Scores = self:Add("DScrollPanel")
        self.Scores:Dock(FILL)
        self.Scores:DockMargin(0, 0, 0, 10)

        local scrollBar = self.Scores:GetVBar()
        scrollBar:DockMargin( -5, 0, 0, 0)
        function scrollBar:Paint(w, h)
            surface.SetDrawColor(10, 10, 10, 100)
            surface.DrawOutlinedRect(0, 0, w - 1, h - 1)
        end
        function scrollBar.btnGrip:Paint(w, h)
            draw.RoundedBox(0, 0, 0, w, h, Color(150, 200, 150, 150))
        end

        self.Title = self.Scores:Add(PLAYER_LINE_TITLE)
        self.Name:SetText("Base Wars")
    end,

    PerformLayout = function(self)
        self:SetSize(700, ScrH() - 100)
        self:SetPos(ScrW() / 2 - 700 / 2, 50)
    end,

    Paint = function(self, w, h)
        draw.RoundedBox(8, 0, 0, w, h, Color(10, 10, 10, 200))
    end,

    Think = function(self, w, h)
        for id, ply in pairs(player.GetAll()) do
            if (IsValid(ply.ScoreEntry)) then continue end

            ply.ScoreEntry = vgui.CreateFromTable(PLAYER_LINE, ply.ScoreEntry)
            ply.ScoreEntry:Setup(ply)

            self.Scores:AddItem(ply.ScoreEntry)
        end
    end
}

SCORE_BOARD = vgui.RegisterTable(SCORE_BOARD, "EditablePanel")

function GM:ScoreboardShow()
    if (!IsValid(Scoreboard)) then
        Scoreboard = vgui.CreateFromTable(SCORE_BOARD)
    end
    if (IsValid(Scoreboard)) then
        Scoreboard:Show()

        // Lines to enable and disable the mouse
        Scoreboard:MakePopup()
        //Scoreboard:SetKeyboardInputEnabled(true)
    end
end

function GM:ScoreboardHide()
    if (IsValid(Scoreboard)) then
        Scoreboard:Hide()
    end
end

-- Commariser
function commarise(amount)
	local formatted = amount
	while true do
		formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
		if (k==0) then
			break
		end
	end
	return formatted
end
